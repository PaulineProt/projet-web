//fonction qui affiche sur la page le chrono et affiche un message si le joueur n'a pas réussi à temps
var decompte = function(i,j){
  document.getElementById("decompte").innerHTML = j + " min " + i + " s";
  if(i==0 && j==0){
    alert("Vous n'avez pas resolu les enigmes à temps. votre chaine est donc perdue pour toujours...");
    window.location.href = 'accueil.html';
  }
}

var temp = 0;

//fonction permettant de décrementer le chrono toutes les secondes
var decrement = function(){
  j = 14;
  while(j>=0){
    for (var i=59; i>-1; i--){
      setTimeout(function(s,m){
        return function(){
          //lance la fonction décompte pour que l'affichage se modifie aussi toutes les secondes
          decompte(s,m);
        }
      }(i,j), temp);
      temp+=1000;
    }
    j--;
  }
}

//lance la décrementation du chrono
decrement();
