//affichage de la carte
var mymap = L.map('mapid').setView([48.84, 2.58], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//cache la div indice
$('#indice').hide()

//permet d'ajouter le score final dans la base de donnée sans relancer la page.
function sendData()
{
  var nom = document.getElementById("nom").value;
  var decompte = document.getElementById("decompte").innerHTML;
  $.ajax({
    type: 'post',
    url: 'php/scores.php',
    data: {nom:nom,decompte:decompte},
  });
  window.location.href = 'accueil.html'

  return false;
}

var valeur = 1;
var liste_objets = [];
var obj_trouves = document.getElementById('objets_trouves');
var indice = document.getElementById('indice');
var ensg = L.marker([48.84, 2.58]);

//ajoute le premier marker
ensg.addTo(mymap)
  .bindPopup('Rends toi là où tout a commencé');

//lance le jeu
sendRequest(1);

indice.innerHTML = "trouve l'endroit où a été filmé la première vidéo YouTube";

//affiche un formulaire dans un popup.
var template = "<form id='popup-form'> <label for='code_secret'>Pour trouver la solution entoure toi des meilleur... </label> <input id='code_secret' type='string'/><button id='button-submit' type='button'>OK</button> </form>";

//fonction permettant de continuer le jeu une fois que le joueur a entré le bon mot de passe dans le formulaire du popup
function layerClickHandler (e) {

  var code = L.DomUtil.get('code_secret');

  var buttonSubmit = L.DomUtil.get('button-submit');

  L.DomEvent.on(buttonSubmit, 'click', function (e) {
    if(code.value == "squeezie20"){
      liste_objets.push("ok");
      console.log(liste_objets);
      if(liste_objets.includes("ok") && liste_objets.includes("images/P3.png")){
         sendRequest(7);
       }
    }
  });
}

//fonction recursive contenant tout le jeu, le paramètre valeur est la valeur de l'id du marker, il se modifie au cours du jeu
function sendRequest(valeur){
  //recupération du json créé dans objets.php
  fetch("php/objets.php?id="+valeur)
    .then(contenu => contenu.json())
    .then(contenu => {
      //ajoute une image au marker
      var posPopup = contenu.icone_larg / 2;
      console.log(posPopup);
      var Icon = new L.Icon({
            iconUrl: contenu.icone_image,
            iconSize:     [contenu.icone_larg,contenu.icone_long],
            iconAnchor:   [contenu.icone_positionX, contenu.icone_positionX],
            popupAnchor:  [posPopup, 0]
    });
      var marker=L.marker([contenu.position_x, contenu.position_y],{icon: Icon});

      //gestion du zoom, le marker ne s'affiche qui si le niveau de zoom atteint une certaine valeur
      mymap.on('zoomend' , function (e) {
          if (mymap.getZoom()>contenu.zoom)
          {
          if (valeur==5){
            //ajoute le formulaire au popup
            marker.addTo(mymap)
              .bindPopup(template);
            marker.on('click', layerClickHandler);
          }

          else if (valeur==4 || valeur ==7){
            //s'assure que le marker s'affiche bien au dessus de l'ancien
            marker.addTo(mymap)
              .bindPopup(contenu.indice)
              .setZIndexOffset(1000);
          }

          else {
            marker.addTo(mymap)
                .bindPopup(contenu.indice);
          }

          }
          else {
              //retire le marker si la carte est trop dezoomée
              marker.remove();
          }

        });

        //gère les actions en fonction du click sur le marker
        marker.on('click', function(e) {

          //ajoute l'image de l'objet récupéré à coté de la carte
          if ( contenu.icone_image != "images/boitefermee.png" && contenu.icone_image != "images/youtube.png" && liste_objets.includes(contenu.icone_image)==false){
            liste_objets.push(contenu.icone_image);
            $("#objets_trouves").append("<img src="+ contenu.icone_image+" id=img"+contenu.id+">");
          }
          //qjoute le marker suivant en appelant la fonction elle même avec une autre valeur en paramètre si le joueur a bien récupéré les bons objets
          if(liste_objets.includes("images/qrcode1.png")){
            console.log(liste_objets);
            ensg.remove();
            sendRequest(2);
            indice.innerHTML = "le siège de YouTube doit bien avoir un indice pour toi...";
            sendRequest(3);
          }
          if(liste_objets.includes("images/cle_google.png")){
            console.log(liste_objets);
            liste_objets.pop();
            sendRequest(4);
          }
          if(liste_objets.includes("images/P2.png")){
            console.log(liste_objets);
            $("#img3").remove();
            sendRequest(5);
            sendRequest(6);
            indice.innerHTML = "Chez toi c'est là où le jeu a commencé, au passage tu verras peut être Levallois, il y a pas mal de gros Youtubeurs là bas apparemment...";

          }
          //modifie la mise en page quand le joueur a fini
          if(liste_objets.includes("ok") && liste_objets.includes("images/P4.png")){
            var content = "<p>Félicitations !</p><p>Tu as désormais accès à la chaine qui te donnera le salut.<br>Scanne le QRCODE, en t'abonnant, ta chaine sera sauvée.<p>Rentre ton nom pour enregistrer ton score !<br>Dépèche toi, le temps file !</p><p><img src='images/QRCODE.png' id=end></p><p><audio controls src='images/Rubrique.mp3'></audio></p><form method='post' onsubmit='return sendData();'><p><label>Nom: <input type='text' id='nom' name='nom'></label></p></div><input type='submit' name='envoi' class='bouton'></form></p>";
            //cache l'affichage actuel
            $("#jeu").hide();
            //affiche la nouvelle mise en page
            $("#result").append(content);
          }
        });
    })
}
