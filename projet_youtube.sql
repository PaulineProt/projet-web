-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2020 at 02:41 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projet_youtube`
--

-- --------------------------------------------------------

--
-- Table structure for table `objets`
--

CREATE TABLE `objets` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `position_x` double NOT NULL,
  `position_y` double NOT NULL,
  `zoom` int(11) NOT NULL,
  `icone_image` text NOT NULL,
  `icone_long` double NOT NULL,
  `icone_larg` double NOT NULL,
  `icone_positionX` double NOT NULL,
  `icone_positionY` double NOT NULL,
  `indice` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objets`
--

INSERT INTO `objets` (`id`, `nom`, `position_x`, `position_y`, `zoom`, `icone_image`, `icone_long`, `icone_larg`, `icone_positionX`, `icone_positionY`, `indice`) VALUES
(1, 'qrcode_zoo', 32.73, -117.15, 13, 'images/qrcode1.png', 60, 60, 0, 0, 'Tu ne trouveras plus rien ici, va dans nos bureaux.'),
(2, 'youtube', 37.628146, -122.426427, 13, 'images/youtube.png', 60, 60, 0, 0, 'Locaux fermé. La clé est chez le big boss.'),
(3, 'cle_google', 37.42233657836914, -122.08436584472656, 13, 'images/cle_google.png', 60, 60, 0, 0, 'Tu as obtenu la clé. Retourne chercher ce que tu n\'as pas pu prendre.'),
(4, 'qrcode_youtube', 37.628146, -122.426427, 13, 'images/P2.png', 60, 60, 0, 0, 'Tu as ouvert avec la clé; Rentre chez toi !!'),
(5, 'boite_ENSG', 48.841023, 2.5873236, 13, 'images/boitefermee.png', 60, 60, 0, 0, 'Pour trouver la solution entoure toi des meilleurs...'),
(6, 'qrcode_Webedia', 48.896419525146484, 2.2855660915374756, 13, 'images/P3.png', 60, 60, 0, 0, 'Mon premier est le nom d\'un youtubeur français à 15 millions d\'abonnés, mon deuxième est l\'année du covid.\n N\'oublie pas de récupérer ton morceau de QRcode !!'),
(7, 'qrcode_ENSG', 48.841023, 2.5873236, 1, 'images/P4.png', 60, 60, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `min` int(11) DEFAULT NULL,
  `sec` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`id`, `nom`, `min`, `sec`) VALUES
(21, 'Iris', 11, 30),
(22, 'Pauline', 12, 3),
(23, 'Maxime', 13, 59),
(24, 'Emma', 10, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `score`
--
ALTER TABLE `score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
